﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void tabulationButtonClick(object sender, EventArgs e) {
            resultListBox.Items.Clear();

            double a = inputValue("a", aTextBox);
            if (double.IsNaN(a)) return;
            double b = inputValue("b", bTextBox);
            if (double.IsNaN(b)) return;
            double h = inputValue("h", hTextBox);
            if (double.IsNaN(h)) return;

            for (double x = a; x <= b; x += h) {
                double y = Math.Pow(Math.Cos(Math.Pow(x, 2)), 3) / (1.5 * x + 2);
                resultListBox.Items.Add($"x = {x:F3}  |  y = {y:F3}");
            }
        }

        private double inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return double.NaN;
            }

            try {
                return double.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                return double.NaN;
            }
        }
    }
}
