﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hintSpanLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.hintALabel = new System.Windows.Forms.Label();
            this.aTextBox = new System.Windows.Forms.TextBox();
            this.bTextBox = new System.Windows.Forms.TextBox();
            this.hintBLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.hTextBox = new System.Windows.Forms.TextBox();
            this.hintHLabel = new System.Windows.Forms.Label();
            this.resultListBox = new System.Windows.Forms.ListBox();
            this.tabulationButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // hintSpanLabel
            // 
            this.hintSpanLabel.AutoSize = true;
            this.hintSpanLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hintSpanLabel.Location = new System.Drawing.Point(33, 25);
            this.hintSpanLabel.Margin = new System.Windows.Forms.Padding(24, 16, 0, 0);
            this.hintSpanLabel.Name = "hintSpanLabel";
            this.hintSpanLabel.Size = new System.Drawing.Size(239, 24);
            this.hintSpanLabel.TabIndex = 0;
            this.hintSpanLabel.Text = "Введіть проміжок [a, b]";
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultLabel.Location = new System.Drawing.Point(342, 25);
            this.resultLabel.Margin = new System.Windows.Forms.Padding(64, 16, 24, 0);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(112, 24);
            this.resultLabel.TabIndex = 1;
            this.resultLabel.Text = "Результат";
            // 
            // hintALabel
            // 
            this.hintALabel.AutoSize = true;
            this.hintALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hintALabel.Location = new System.Drawing.Point(33, 65);
            this.hintALabel.Margin = new System.Windows.Forms.Padding(24, 8, 0, 0);
            this.hintALabel.Name = "hintALabel";
            this.hintALabel.Size = new System.Drawing.Size(21, 24);
            this.hintALabel.TabIndex = 2;
            this.hintALabel.Text = "a";
            // 
            // aTextBox
            // 
            this.aTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.aTextBox.Location = new System.Drawing.Point(25, 93);
            this.aTextBox.Margin = new System.Windows.Forms.Padding(16, 4, 0, 0);
            this.aTextBox.Name = "aTextBox";
            this.aTextBox.Size = new System.Drawing.Size(100, 29);
            this.aTextBox.TabIndex = 3;
            // 
            // bTextBox
            // 
            this.bTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bTextBox.Location = new System.Drawing.Point(178, 93);
            this.bTextBox.Margin = new System.Windows.Forms.Padding(16, 4, 0, 0);
            this.bTextBox.Name = "bTextBox";
            this.bTextBox.Size = new System.Drawing.Size(100, 29);
            this.bTextBox.TabIndex = 5;
            // 
            // hintBLabel
            // 
            this.hintBLabel.AutoSize = true;
            this.hintBLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hintBLabel.Location = new System.Drawing.Point(186, 65);
            this.hintBLabel.Margin = new System.Windows.Forms.Padding(24, 8, 0, 0);
            this.hintBLabel.Name = "hintBLabel";
            this.hintBLabel.Size = new System.Drawing.Size(22, 24);
            this.hintBLabel.TabIndex = 4;
            this.hintBLabel.Text = "b";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(33, 154);
            this.label1.Margin = new System.Windows.Forms.Padding(24, 32, 0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ввудіть крок h";
            // 
            // hTextBox
            // 
            this.hTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hTextBox.Location = new System.Drawing.Point(25, 214);
            this.hTextBox.Margin = new System.Windows.Forms.Padding(16, 4, 0, 0);
            this.hTextBox.Name = "hTextBox";
            this.hTextBox.Size = new System.Drawing.Size(100, 29);
            this.hTextBox.TabIndex = 8;
            // 
            // hintHLabel
            // 
            this.hintHLabel.AutoSize = true;
            this.hintHLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hintHLabel.Location = new System.Drawing.Point(33, 186);
            this.hintHLabel.Margin = new System.Windows.Forms.Padding(24, 8, 0, 0);
            this.hintHLabel.Name = "hintHLabel";
            this.hintHLabel.Size = new System.Drawing.Size(22, 24);
            this.hintHLabel.TabIndex = 7;
            this.hintHLabel.Text = "h";
            // 
            // resultListBox
            // 
            this.resultListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultListBox.FormattingEnabled = true;
            this.resultListBox.ItemHeight = 24;
            this.resultListBox.Location = new System.Drawing.Point(334, 65);
            this.resultListBox.Margin = new System.Windows.Forms.Padding(56, 16, 16, 40);
            this.resultListBox.Name = "resultListBox";
            this.resultListBox.Size = new System.Drawing.Size(255, 316);
            this.resultListBox.TabIndex = 9;
            // 
            // tabulationButton
            // 
            this.tabulationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tabulationButton.AutoSize = true;
            this.tabulationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabulationButton.Location = new System.Drawing.Point(25, 343);
            this.tabulationButton.Margin = new System.Windows.Forms.Padding(16, 0, 16, 40);
            this.tabulationButton.Name = "tabulationButton";
            this.tabulationButton.Padding = new System.Windows.Forms.Padding(8, 2, 8, 2);
            this.tabulationButton.Size = new System.Drawing.Size(155, 38);
            this.tabulationButton.TabIndex = 10;
            this.tabulationButton.Text = "Табулювання";
            this.tabulationButton.UseVisualStyleBackColor = true;
            this.tabulationButton.Click += new System.EventHandler(this.tabulationButtonClick);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(367, 403);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(238, 22);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ботвін О. Ю. гр. ЗІПЗ-22-1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 436);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tabulationButton);
            this.Controls.Add(this.resultListBox);
            this.Controls.Add(this.hTextBox);
            this.Controls.Add(this.hintHLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bTextBox);
            this.Controls.Add(this.hintBLabel);
            this.Controls.Add(this.aTextBox);
            this.Controls.Add(this.hintALabel);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.hintSpanLabel);
            this.MinimumSize = new System.Drawing.Size(628, 460);
            this.Name = "Form1";
            this.Text = "Лабораторна робота No2. Завдання 1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label hintSpanLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Label hintALabel;
        private System.Windows.Forms.TextBox aTextBox;
        private System.Windows.Forms.TextBox bTextBox;
        private System.Windows.Forms.Label hintBLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox hTextBox;
        private System.Windows.Forms.Label hintHLabel;
        private System.Windows.Forms.ListBox resultListBox;
        private System.Windows.Forms.Button tabulationButton;
        private System.Windows.Forms.Label label2;
    }
}

