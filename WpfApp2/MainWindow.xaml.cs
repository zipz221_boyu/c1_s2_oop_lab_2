﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void resultButtonClick(object sender, RoutedEventArgs e) {
            canvas.Children.Clear();

            int x = inputValue("x", xTextBox);
            if (x == 0) return;
            int y = inputValue("y", yTextBox);
            if (y == 0) return;

            int value = x % y;

            if (value == 1) canvas.Children.Add(shape(new Ellipse(), Brushes.Red));
            else if (value == 2) canvas.Children.Add(shape(new Rectangle(), Brushes.Blue));
        }


        private int inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return 0;
            }

            try {
                return int.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return 0;
            }
        }

        private Shape shape(Shape shape, Brush color) {
            shape.Width = canvas.Width;
            shape.Height = canvas.Height;
            shape.Stroke = color;
            shape.StrokeThickness = 2;
            return shape;
        }

        private Ellipse ellipse() {
            Ellipse shape = new Ellipse();
            shape.Width = canvas.Width - 24;
            shape.Height = canvas.Height - 24;
            shape.Stroke = Brushes.Red;
            shape.StrokeThickness = 2;
            return shape;
        }

        private Rectangle rectangle() {
            Rectangle shape = new Rectangle();
            shape.Width = canvas.Width - 24;
            shape.Height = canvas.Height - 24;
            shape.Stroke = Brushes.Blue;
            shape.StrokeThickness = 2;
            return shape;
        }
    }
}
