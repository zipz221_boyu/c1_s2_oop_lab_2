﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void resultButtonClick(object sender, EventArgs e) {
            graphics().Clear(System.Drawing.SystemColors.Control);

            int x = inputValue("x", xTextBox);
            if (x == 0) return;
            int y = inputValue("y", yTextBox);
            if (y == 0) return;

            int value = x % y;

            if (value == 1) graphics().DrawEllipse(pen(Color.Red), rectangle());
            else if (value == 2) graphics().DrawRectangle(pen(Color.Blue), rectangle());
        }

        private int inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }

            try {
                return int.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                return 0;
            }
        }

        private Graphics graphics() {
            return pictureBox.CreateGraphics();
        }

        private Pen pen(Color color) {
            Pen pen = new Pen(color);
            pen.Width = 2;
            return pen;
        }

        private Rectangle rectangle() {
            return new Rectangle(10, 10, pictureBox.Width - 24, pictureBox.Height - 24);
        }
    }
}
