﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void tabulationButtonClick(object sender, RoutedEventArgs e) {
            resultListBox.Items.Clear();

            double a = inputValue("a", aTextBox);
            if (double.IsNaN(a)) return;
            double b = inputValue("b", bTextBox);
            if (double.IsNaN(b)) return;
            double h = inputValue("h", hTextBox);
            if (double.IsNaN(h)) return;

            for (double x = a; x <= b; x += h) {
                double y = Math.Pow(Math.Cos(Math.Pow(x, 2)), 3) / (1.5 * x + 2);
                resultListBox.Items.Add($"x = {x:F3}  |  y = {y:F3}");
            }
        }

        private double inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return double.NaN;
            }

            try {
                return double.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return double.NaN;
            }
        }
    }
}
